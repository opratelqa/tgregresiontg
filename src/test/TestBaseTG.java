package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class TestBaseTG extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 10);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	
	
	
	String chooseYourUser ="it@opratel.com"; //Usuario Premium	
	//String chooseYourUser ="comprovidanueva@gmail.com"; //Usuario No Premium
	
	
	//String passAmbiente ="123456"; //Contraseña QA it@opratel.com 
	String passAmbiente ="t0g4.0pr4"; //Contraseña PRODUCCION
	//String passAmbiente ="tg*0pr4t31"; //Contraseña DV4
	
	
	
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="https://qa."; //AMBIENTE QA
	
	//String apuntaA ="https://dev4."; //AMBIENTE DEV7
	
	String apuntaA ="https://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		EDPTG03LogIn TP = new EDPTG03LogIn(driver);
		TP.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP.LogOutTodosGamers(apuntaA);
	}
	     
	
	@Test (priority=1) 
	public void MiPerfil()	{
	EDPTG04MiPerfil TP1 = new EDPTG04MiPerfil(driver);
	TP1.LogInTodosGamers(passAmbiente,chooseYourUser); 
		TP1.MiPerfilTodosGamers(apuntaA);
		TP1.LogOutTodosGamers(apuntaA);
	}   
	
	
	@Test (priority=2)
	public void EditarCuenta() {
			EDPTG05EditarCuenta TP2 = new EDPTG05EditarCuenta(driver);
			TP2.LogInTodosGamers(passAmbiente,chooseYourUser); 
			TP2.EditarCuenta(apuntaA);
			TP2.LogOutTodosGamers(apuntaA);
	}
	 
	
	@Test (priority=3)
	public void HacerPremiumLaCuenta() {	
		EDPTG06HacerPremiumLaCuenta TP3 = new EDPTG06HacerPremiumLaCuenta(driver);
		TP3.LogInTodosGamers(passAmbiente,chooseYourUser); 
		TP3.HacerPremiumLaCuenta(apuntaA);
		TP3.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=4)
	public void MisEquipos() {
		EDPTG07MisEquipos TP4 = new EDPTG07MisEquipos(driver);
		TP4.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP4.MisEquipos(apuntaA);
		TP4.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=5)
	public void MisTorneos() {
	EDPTG08MisTorneos TP5 = new EDPTG08MisTorneos(driver);
		TP5.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP5.MisTorneos(apuntaA);
		TP5.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=6)
	public void GenerarCodigoReferidos() {
	EDPTG09GenerarCodigoReferidos TP6 = new EDPTG09GenerarCodigoReferidos(driver);
	TP6.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP6.GenerarCodigoReferidos(apuntaA);
		TP6.LogOutTodosGamers(apuntaA);
	} 
	
	//Agregar EDPTG10UtilizarCodigoReferido
	
	@Test (priority=7)
	public void Inicio() {
	EDPTG11Inicio TP8 = new EDPTG11Inicio(driver);
		TP8.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP8.Inicio();
		TP8.LogOutTodosGamers(apuntaA);
	} 
	
	
	@Test (priority=8)
	public void Torneos() {
	EDPTG12Torneos TP9 = new EDPTG12Torneos(driver);
	TP9.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP9.Torneos();
		TP9.LogOutTodosGamers(apuntaA);
	} 
	
	
	@Test (priority=9)
	public void CrearTorneoFree() {
	EDPTG13CrearTorneoFree TP10 = new EDPTG13CrearTorneoFree(driver);
		TP10.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP10.CrearTorneo1(apuntaA);
		TP10.CrearTorneo2();
		TP10.LogOutTodosGamers(apuntaA);
	} 
	  
	
	@Test (priority=10)
	public void CrearTorneoPremium() { 
		EDPTG14CrearTorneoPremium TP11 = new EDPTG14CrearTorneoPremium(driver);
		TP11.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP11.CrearTorneo1(apuntaA);
		TP11.CrearTorneo2();
		TP11.LogOutTodosGamers(apuntaA);
	} 
	
	
	@Test (priority=11)
	public void consultaTorneo() {
	EDPTG17ConsultaTorneo TP12 = new EDPTG17ConsultaTorneo(driver);
		TP12.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP12.Torneos(apuntaA);
		TP12.consulta();
		TP12.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=12)
	public void editarTorneo() {
	EDPTG18EditarTorneo TP13 = new EDPTG18EditarTorneo(driver);
		TP13.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP13.Torneos(apuntaA);
		TP13.Editar();
		TP13.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=13)
	public void activarTorneo() {
	EDPTG19ActivarTorneo TP14 = new EDPTG19ActivarTorneo(driver);
	TP14.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP14.Torneos(apuntaA);
		TP14.activar();
		TP14.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=14)
	public void desactivarTorneo() {
	EDPTG20DesactivarTorneo TP15 = new EDPTG20DesactivarTorneo(driver);
	TP15.LogInTodosGamers(passAmbiente,chooseYourUser);
	TP15.Torneos(apuntaA);
		TP15.desactivar();
		TP15.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=15)
	public void administrarPartida() {
	EDPTG21AdministrarPartidas TP16 = new EDPTG21AdministrarPartidas(driver);
	TP16.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP16.Torneos(apuntaA);
		TP16.administrar();
		TP16.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=16)
	public void EliminarTorneo() {
		EDPTG22EliminarTorneo TP17 = new EDPTG22EliminarTorneo(driver);
		TP17.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP17.Torneos(apuntaA);
		TP17.eliminar();
		TP17.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=17)
	public void seccionEquipos() {
		EDPTG23Equipos TP18 = new EDPTG23Equipos(driver);
		TP18.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP18.Equipos(apuntaA);
		TP18.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=18)
	public void crearEquipo() {
		EDPTG24CrearEquipo TP19 = new EDPTG24CrearEquipo(driver);
		TP19.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP19.CrearEquipo(apuntaA);
		TP19.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=19)
	public void EquipoPorJuego() {
		EDPTG25ConsultaEquipoPorJuego TP20 = new EDPTG25ConsultaEquipoPorJuego(driver);
		TP20.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP20.ConsultaEquipo(apuntaA);
		TP20.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=20)
	public void UnirmeEquipo() {
		EDPTG26UnirmeAEquipo TP21 = new EDPTG26UnirmeAEquipo(driver);
		TP21.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP21.UnirmeAEquipoTG(apuntaA);
		TP21.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=21)
	public void EliminaEquipo() {
		EDPTG27EliminarEquipo TP22 = new EDPTG27EliminarEquipo(driver);
		TP22.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP22.EliminarEquipoTG();
		TP22.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=22)
	public void misiones() {
	EDPTG28Misiones TP23 = new EDPTG28Misiones(driver);
		TP23.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP23.Misiones();
			TP23.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=23)
	public void Consultamisiones() {
		EDPTG29ConsultaMision TP24 = new EDPTG29ConsultaMision(driver);
		TP24.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP24.Misiones(apuntaA);
			TP24.LogOutTodosGamers(apuntaA);
			//verificar validacion de este TS
	}	
	
	
	@Test (priority=24)
	public void crearMisiones() {
		EDPTG30CrearMision TP25 = new EDPTG30CrearMision(driver);
		TP25.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP25.CrearMisionStep1(apuntaA);
			TP25.CrearMisionStep2();
			TP25.CrearMisionStep3();
			//verificar validacion de este TS
			TP25.LogOutTodosGamers(apuntaA);
		}
	
	@Test (priority=25)
	public void EliminarMisiones() {
	EDPTG32EliminarMision TP26 = new EDPTG32EliminarMision(driver);
	TP26.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP26.EliminarMision(apuntaA);
		TP26.LogOutTodosGamers(apuntaA);
	
		}
	
	@Test (priority=26)
	public void rankingTG() {
	EDPTG33Ranking TP27 = new EDPTG33Ranking(driver);
	TP27.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP27.RankingSeccion();
		TP27.LogOutTodosGamers(apuntaA);
		}
	
	@Test (priority=27)
	public void ConsultaRankingTG() {
	EDPTG34ConsultaRanking TP28 = new EDPTG34ConsultaRanking(driver);
	TP28.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP28.ConsultaRanking();
		TP28.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=28)
	public void CSGOLOBBY() {
	EDPTG35CSGOLOBBY TP29 = new EDPTG35CSGOLOBBY(driver);
	TP29.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP29.CSGOLOBBYSECCION();
		TP29.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=29)
	public void CSGOPUBLICO() {
	EDPTG36UNIRSECSGOLOBBYPUBLICO TP30 = new EDPTG36UNIRSECSGOLOBBYPUBLICO(driver);
	TP30.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP30.UNIRSECSGOLOBBYPUB();
		TP30.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=30)
	public void CSGOPRIVADO() {
	EDPTG37UNIRSECSGOLOBBYPV TP31 = new EDPTG37UNIRSECSGOLOBBYPV(driver);
	TP31.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP31.UNIRSECSGOLOBBYPV();
		TP31.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=31)
	public void noticias() {
	EDPTG38NOTICIAS TP32 = new EDPTG38NOTICIAS(driver);
	TP32.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP32.noticias();
		TP32.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=32)
	public void noticiaspopulares() {
		EDPTG39NOTICIASPOPULARES TP33 = new EDPTG39NOTICIASPOPULARES(driver);
		TP33.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP33.noticias();
			TP33.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=33)
	public void noticiasrecientes() {
	EDPTG40NOTICIASRECIENTES TP34 = new EDPTG40NOTICIASRECIENTES(driver);
	TP34.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP34.noticias();
		TP34.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=34)
	public void CMSTemporadasTG() {
	EDPTG41CMSTEMPORADAS TP35 = new EDPTG41CMSTEMPORADAS(driver);
	TP35.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP35.CMSTemporadas(apuntaA);
		TP35.LogOutTodosGamers(apuntaA);
	}
	 
	@Test (priority=35)
	public void CMSTempCrearRanking() {
	EDPTG42CMSTEMPORADASCREARTIPORANKING TP36 = new EDPTG42CMSTEMPORADASCREARTIPORANKING(driver);
	TP36.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP36.CMSTemporadas(apuntaA);
		TP36.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=36)
	public void CMSTempSubBaj() {
	EDPTG43CMSTEMPORADASSYB TP37 = new EDPTG43CMSTEMPORADASSYB(driver);
	TP37.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP37.CMSTemporadas(apuntaA);
		TP37.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=37)
	public void CMSTempEliminar() {
	EDPTG44CMSTEMPORADASELIMINAR TP38 = new EDPTG44CMSTEMPORADASELIMINAR(driver);
	TP38.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP38.CMSTemporadas(apuntaA);
		TP38.LogOutTodosGamers(apuntaA);
	} 
	 
	
	@Test (priority=38)
	public void CMSAsociacionR() {
	EDPTG45CMSAsociacionRanking TP39 = new EDPTG45CMSAsociacionRanking(driver);
	TP39.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP39.CMSAsociacionRanking(apuntaA);
		TP39.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=39)
	public void CMSLISTNOT() {
	EDPTG50CMSListNotification TP40 = new EDPTG50CMSListNotification(driver);
	TP40.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP40.CMSListNotification(apuntaA);
		TP40.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=40)
	public void CMSNOTIFICACION() {
	EDPTG51CMSNotificacion TP41 = new EDPTG51CMSNotificacion(driver);
	TP41.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP41.CMSNotification(apuntaA);
		TP41.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=41)
	public void CMSSendsNOTIFICACION() {
	EDPTG52CMSNotificacionEnviarnotificacion TP42 = new EDPTG52CMSNotificacionEnviarnotificacion(driver);
	TP42.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP42.CMSEnviarNotification(apuntaA);
			TP42.detallesDeLaNotificacion();
			TP42.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=42)
	public void CMSReqUsuario() {
	EDPTG53CMSReqUsuario TP43 = new EDPTG53CMSReqUsuario(driver);
	TP43.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP43.CMSRUsuario(apuntaA);
		TP43.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=43)
	public void CMSLoginAsTest() {
	EDPTG54CMSLogInAs TP44 = new EDPTG54CMSLogInAs(driver);
	TP44.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP44.CMSLoginAs(apuntaA);
		TP44.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=44)
	public void CMSPAYPALLSUS() {
	EDPTG55CMSPaypallSuscripcion TP45 = new EDPTG55CMSPaypallSuscripcion(driver);
	TP45.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP45.CMSPaypallSuscripcion(apuntaA);
		TP45.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=45)
	public void CMSPAYPALLSUSCREAR() {
	EDPTG56CMSPaypallSuscripcionCrear TP46 = new EDPTG56CMSPaypallSuscripcionCrear(driver);
	TP46.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP46.CMSPaypallSuscripcion(apuntaA);
		TP46.CMSPaypallSuscripcionCrear();
		TP46.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=46)
	public void CMSPAYPALLSUSEDITAR() {
	EDPTG57CMSPaypallSuscripcionEditar TP47 = new EDPTG57CMSPaypallSuscripcionEditar(driver);
	TP47.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP47.CMSPaypallSuscripcion(apuntaA);
		TP47.CMSPaypallSuscripcionEditar();
		TP47.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=47)
	public void CMSPAYPALLSUSELIMINAR() {
	EDPTG58CMSPaypallSuscripcionEliminar TP48 = new EDPTG58CMSPaypallSuscripcionEliminar(driver);
		TP48.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP48.CMSPaypallSuscripcion(apuntaA);
			TP48.CMSPaypallSuscripcionEliminar();
			TP48.LogOutTodosGamers(apuntaA);
	} 
		 
	 
	@Test (priority=48)
	public void CMSLoadDashboard() {
	EDPTG59CMSCargarDashboard TP49 = new EDPTG59CMSCargarDashboard(driver);
		TP49.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP49.CMSCD(apuntaA);
			TP49.LogOutTodosGamers(apuntaA);
	}
	
	//@Test (priority=49) ROMPE EL DASHBOARD	 DE TG
	public void CMSLoadDashboardCrear() {
	EDPTG60CMSCargarDashboardCrear TP50 = new EDPTG60CMSCargarDashboardCrear(driver);
		TP50.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP50.CMSCargarDashboardCrear(apuntaA);
			TP50.CMSCargarDashboardCrear2();
			TP50.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=50)
	public void CMSNews() {
	EDPTG63CMSNoticias TP53 = new EDPTG63CMSNoticias(driver);
			TP53.LogInTodosGamers(passAmbiente,chooseYourUser);
				TP53.CMSNotocias(apuntaA);
				TP53.LogOutTodosGamers(apuntaA);
	}
	 
	@Test (priority=51)
	public void CMSNewsCrear() {
	EDPTG64CMSNoticiasCrear TP54 = new EDPTG64CMSNoticiasCrear(driver);
		TP54.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP54.CMSNotocias(apuntaA);
				TP54.CMSNotociasCrear();
				TP54.LogOutTodosGamers(apuntaA);
	}

	@Test (priority=52)
	public void CMSNewsEditar() {
	EDPTG65CMSNoticiasEditar TP55 = new EDPTG65CMSNoticiasEditar(driver);
		TP55.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP55.CMSNotocias(apuntaA);
				TP55.CMSNotociasEditar();
				TP55.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=53)
	public void CMSNewsEliminar() {
	EDPTG66CMSNoticiasEliminar TP56 = new EDPTG66CMSNoticiasEliminar(driver);
		TP56.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP56.CMSNotocias(apuntaA);
				TP56.CMSNotociasEliminar();
				TP56.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=54)
	public void CMSGraceDayTest() {
	EDPTG67CMSGraceDay TP57 = new EDPTG67CMSGraceDay(driver);
		TP57.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP57.CMSGraceDay(apuntaA);
			TP57.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=55)
	public void CMSGraceDayCrear() {
	EDPTG68CMSGraceDayCrear TP58 = new EDPTG68CMSGraceDayCrear(driver);
	TP58.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP58.CMSGraceDay(apuntaA);
			TP58.CMSGraceDayCrear();
			TP58.LogOutTodosGamers(apuntaA);
	}
	
	//@Test (priority=56) ver xq no hace click en btn editar
	public void CMSGraceDayEditar() {
	EDPTG69CMSGraceDayEditar TP59 = new EDPTG69CMSGraceDayEditar(driver);
	TP59.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP59.CMSGraceDay(apuntaA);
			TP59.CMSGraceDayEditar(); 
	}
	
	
	@Test (priority=57)
	public void CMSMENU() {
	EDPTG71CMSMenu TP60 = new EDPTG71CMSMenu(driver);
	TP60.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP60.CMSmenu(apuntaA);
		TP60.LogOutTodosGamers(apuntaA);
	} 
	
	//@Test (priority=58)
	public void CMSMENUCREAR() {
	EDPTG72CMSMenuCrear TP61 = new EDPTG72CMSMenuCrear(driver);
	TP61.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP61.CMSNotocias(apuntaA); 
			TP61.CMSmenuCrear();
			TP61.LogOutTodosGamers(apuntaA);
	}
	
	 
	//@Test (priority=59)
	public void CMSMENUEliminar() {
	EDPTG74CMSMenuEliminar TP62 = new EDPTG74CMSMenuEliminar(driver);
	TP62.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP62.CMSNotocias(apuntaA); 
			TP62.CMSmenuEliminar();
			TP62.LogOutTodosGamers(apuntaA);
	}
	
	
	//@Test (priority=60) ROMPE deben arreglar ruta de cms matchmaking
	public void CMSLobby() {
	EDPTG75CMSLobby TP63 = new EDPTG75CMSLobby(driver);
	TP63.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP63.CMSLobby();
		TP63.LogOutTodosGamers(apuntaA);
	}
	
	//@Test (priority=61) ROMPE deben arreglar ruta cms matchmaking
	public void CMSLobbyConsult() {
	EDPTG76CMSLobbyConsulta TP64 = new EDPTG76CMSLobbyConsulta(driver);
	TP64.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP64.CMSLobby();
		TP64.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=62)
	public void CMSpermisos() {
	EDPTG77CMSPermisos TP65 = new EDPTG77CMSPermisos(driver);
	TP65.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP65.CMSPermisos(apuntaA);
		TP65.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=63)
	public void CMSpermisosCrear() {
	EDPTG78CMSPermisosCrear TP66 = new EDPTG78CMSPermisosCrear(driver);
	TP66.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP66.CMSPermisos(apuntaA);
			TP66.CMSPermisosCrear();
			TP66.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=64)
	public void CMSpermisosEditar() {
	EDPTG79CMSPermisosEditar TP67 = new EDPTG79CMSPermisosEditar(driver);
	TP67.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP67.CMSPermisos(apuntaA);
			TP67.CMSPermisosEditar();
			TP67.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=65)
	public void CMSpermisosEliminar() {
	EDPTG80CMSPermisosEliminar TP68 = new EDPTG80CMSPermisosEliminar(driver);
	TP68.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP68.CMSPermisos(apuntaA);
			TP68.CMSPermisosEliminar();
			TP68.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=66)
	public void CMSRolesTest() {
	EDPTG81CMSRoles TP69 = new EDPTG81CMSRoles(driver);
	TP69.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP69.CMSRoles(apuntaA);
		TP69.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=67)
	public void CMSRolesCrear() {
	EDPTG82CMSRolesCrear TP70 = new EDPTG82CMSRolesCrear(driver);
	TP70.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP70.CMSRoles(apuntaA);
		TP70.CMSRolesCrear();
		TP70.LogOutTodosGamers(apuntaA);
			
	}
	
	
	@Test (priority=68)
	public void CMSRolesEditar() {
	EDPTG83CMSRolesEditar TP71 = new EDPTG83CMSRolesEditar(driver);
	TP71.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP71.CMSRoles(apuntaA);
		TP71.CMSRolesEditar();
		TP71.LogOutTodosGamers(apuntaA);
	}
	 
	@Test (priority=69)
	public void CMSUsuarioTest() {
	EDPTG85CMSUsuarios TP72 = new EDPTG85CMSUsuarios(driver);
	TP72.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP72.CMSUsuario(apuntaA);
		TP72.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=70)
	public void CMSUsuarioEditar() {
	EDPTG86CMSUsuariosEditar TP73 = new EDPTG86CMSUsuariosEditar(driver);
	TP73.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP73.CMSUsuario(apuntaA);
		TP73.CMSUsuarioEditar();
		TP73.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=71)
	public void CMSAsignarRolaUsuario() {
	EDPTG87AsignaRolAUsuario TP74 = new EDPTG87AsignaRolAUsuario(driver);
		TP74.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP74.CMSUsuario(apuntaA);
			TP74.CMSUsuarioAsignaRol();
			TP74.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=72)
	public void CMSTorneo() {
	EDPTG89CMSTorneos TP75 = new EDPTG89CMSTorneos(driver);
		TP75.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP75.CMSTorneo(apuntaA);
			TP75.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=73)
	public void CMSCreaTorneo() {
		EDPTG90CMSTorneosCrear TP76 = new EDPTG90CMSTorneosCrear(driver);
			TP76.LogInTodosGamers(passAmbiente,chooseYourUser);
				TP76.CrearTorneo1(apuntaA);			
				TP76.CrearTorneo2();
				TP76.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=74)
	public void CMSEditaTorneo() {
	EDPTG91CMSTorneosEditar TP77 = new EDPTG91CMSTorneosEditar(driver);
		TP77.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP77.Torneos(apuntaA);
			TP77.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=75)
	public void CMSEliminaTorneo() {
	EDPTG92CMSTorneosEliminar TP78 = new EDPTG92CMSTorneosEliminar(driver);
		TP78.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP78.Torneos(apuntaA);
			TP78.eliminar();
			TP78.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=76)
	public void CMSActivarTorneo() {
	EDPTG93CMSTorneosActivar TP79 = new EDPTG93CMSTorneosActivar(driver);
			TP79.LogInTodosGamers(passAmbiente,chooseYourUser);
				TP79.Torneos(apuntaA);
				TP79.activar();
				TP79.LogOutTodosGamers(apuntaA);
		}
	
	@Test (priority=77)
	public void CMSDesactivarTorneo() {
	EDPTG94CMSTorneosDesactivar TP80 = new EDPTG94CMSTorneosDesactivar(driver);
	TP80.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP80.Torneos(apuntaA);
			TP80.desactivar();
			TP80.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=78)
	public void CMSNuevaMIsion() {
	EDPTG95CMSNuevaMision TP81 = new EDPTG95CMSNuevaMision(driver);
	TP81.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP81.Misiones(apuntaA);
			TP81.LogOutTodosGamers(apuntaA);
	}
	 

	@Test (priority=78)
	public void CMScrearMisiones() {
	EDPTG96CMSNuevaMisionCrear TP82 = new EDPTG96CMSNuevaMisionCrear(driver);
		TP82.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP82.CrearMisionStep1(apuntaA);
			TP82.CrearMisionStep2();
			TP82.CrearMisionStep3();
			//verificar validacion de este TS
			TP82.LogOutTodosGamers(apuntaA);
		}
	
	
	//@Test (priority=79) //agregar un btn de Next Step con un ID :p
	public void CMSEditarMisiones() {
		EDPTG97CMSNuevaMisionEditar TP83 = new EDPTG97CMSNuevaMisionEditar(driver);
		TP83.LogInTodosGamers(passAmbiente,chooseYourUser);
			TP83.CrearMisionStep1(apuntaA);
			TP83.CrearMisionStep2();
			TP83.CrearMisionStep3();
			//verificar validacion de este TS
			TP83.LogOutTodosGamers(apuntaA);
		}
	 
	
	@Test (priority=80)
	public void CMSEliminarMisiones() {
	EDPTG98CMSNuevaMisionEliminar TP84 = new EDPTG98CMSNuevaMisionEliminar(driver);
	TP84.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP84.EliminarMision(apuntaA);
		TP84.LogOutTodosGamers(apuntaA);
	
	}
	
	@Test (priority=81)
	public void CMSPremiosTest() {
		EDPTG99CMSPremios TP85 = new EDPTG99CMSPremios(driver);
	TP85.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP85.CMSPremios(apuntaA);
		TP85.LogOutTodosGamers(apuntaA);
	
	}
	
	@Test (priority=81)
	public void CMSPremiosCrear() {
		EDPTG100CMSPremiosCrear TP85 = new EDPTG100CMSPremiosCrear(driver);
	TP85.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP85.CMSPremios(apuntaA);
		TP85.CMSPremiosCrear(); 
		TP85.LogOutTodosGamers(apuntaA);
	
	}
	
	@Test (priority=82)
	public void CMSPremiosEditar() {
		EDPTG101CMSPremiosEditar TP86 = new EDPTG101CMSPremiosEditar(driver);
	TP86.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP86.CMSPremios(apuntaA);
		TP86.CMSPremiosEditar(); 
		TP86.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=83)
	public void CMSPremiosEliminar() {
	EDPTG102CMSPremiosEliminar TP86 = new EDPTG102CMSPremiosEliminar(driver);
	TP86.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP86.CMSPremios(apuntaA);
		TP86.CMSPremiosEliminar(); 
		TP86.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=84)
	public void CMSTraduccionesTest() {
	EDPTG103CMSTraducciones TP87 = new EDPTG103CMSTraducciones(driver);
	TP87.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP87.CMSTraducciones(apuntaA);
		TP87.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=85)
	public void CMSTutorialesTest() {
	EDPTG104CMSTutoriales TP88 = new EDPTG104CMSTutoriales(driver);
	TP88.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP88.CMSTutoriales(apuntaA);
		TP88.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=86)
	public void CMSTutorialesCrearTest() {
	EDPTG105CMSTutorialesCrear TP89 = new EDPTG105CMSTutorialesCrear(driver);
	TP89.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP89.CMSTutoriales(apuntaA);
		TP89.CMSTutorialesCrear();
		TP89.LogOutTodosGamers(apuntaA);
	}
	
	
	@Test (priority=87)
	public void CMSTutorialesEditarTest() {
	EDPTG106CMSTutorialesEditar TP90 = new EDPTG106CMSTutorialesEditar(driver);
	TP90.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP90.CMSTutoriales(apuntaA);
		TP90.CMSTutorialesEditar();
		TP90.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=88)
	public void CMSTutorialesEliminarTest() {
	EDPTG107CMSTutorialesEliminar TP91 = new EDPTG107CMSTutorialesEliminar(driver);
	TP91.LogInTodosGamers(passAmbiente,chooseYourUser);
		TP91.CMSTutoriales(apuntaA);
		TP91.CMSTutorialesEliminar();
		TP91.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=89)
	public void LogOut() {
	EDPTG109LogOut TP92 = new EDPTG109LogOut(driver);
	TP92.LogInTodosGamers(passAmbiente,chooseYourUser);
	TP92.LogOutTodosGamers(apuntaA);
	}
	
	@Test (priority=90)
	public void idioma() {
	EDPTG108Lenguaje TP93 = new EDPTG108Lenguaje(driver);
	TP93.LogInTodosGamers(passAmbiente,chooseYourUser);
	TP93.lenguaje(apuntaA);
	TP93.LogOutTodosGamers(apuntaA); 
	}
	
	@Test (priority=91)
	public void OfflineTournaments() {
	EDPTG110DeslogueadoTournaments TP94 = new EDPTG110DeslogueadoTournaments(driver);
	TP94.TournamentsOffLine(apuntaA);
	
	}
	
	@Test (priority=92)
	public void OfflineChallenges() {
	EDPTG111DeslogueadoChallenges TP95 = new EDPTG111DeslogueadoChallenges(driver);
	TP95.ChallengesOffLine(apuntaA);
	
	}
	
	@Test (priority=93)
	public void OfflineRanking() {
	EDPTG112DeslogueadoRanking TP96 = new EDPTG112DeslogueadoRanking(driver);
	TP96.RankingOffLine(apuntaA);
	
	}
	
	@Test (priority=94)
	public void NoticiasRanking() {
	EDPTG113DeslogueadoNews TP97 = new EDPTG113DeslogueadoNews(driver);
	TP97.NoticiasOffLine(apuntaA);
	
	}
	
	
	
	
	
	
	
	
	 
	
	//@Test
	public void noSoyHumano() {
		BOTBENJA TPZZ = new BOTBENJA(driver);
		TPZZ.pruebaBot();
		 
	} 
	
	//@Test
	public void rompeChat() {
	ZEDPTG110Chat TPRompe = new ZEDPTG110Chat(driver);
	TPRompe.LogInTodosGamers(passAmbiente,chooseYourUser);
		TPRompe.breakTheChat();
			TPRompe.LogOutTodosGamers();
			 
		}
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */
